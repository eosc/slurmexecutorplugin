SLURM executor plugin for AIRFLOW - README
======================================================

The slurm executor plugin enables the possibility to use
slurm as an executor provided that the scheduler is executed
on a machine where the slurm client is configured properly.


To use the plugin after installing the package change
the AIRFLOW configuration file as such:
```
# The executor class that airflow should use. Choices include
# SequentialExecutor, LocalExecutor, CeleryExecutor, DaskExecutor, KubernetesExecutor
executor = slurm.SlurmExecutor
```

<img src="https://www.egi.eu/wp-content/uploads/2020/01/eu-logo.jpeg" alt="EU Flag" width="80">
<img src="https://www.egi.eu/wp-content/uploads/2020/01/eosc-hub-v-web.png" alt="EOSC-hub logo" height="60">
This work is co-funded by the EOSC-hub project (Horizon 2020) under Grant number 777536. 
