from setuptools import setup
import unittest


def test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover('tests', pattern='test_*.py')
    return test_suite


setup(
    name="slurm-executor-plugin",
    version='0.1',
    description='Slurm executor plugin for AIRFLOW',
    url='https://git.astron.nl/eosc/slurmexecutorplugin/',
    package_dir = {'': 'lib'},
    packages = ['slurm_cli', 'slurm_executor'],
    test_suite = 'setup.test_suite',
    entry_points = {
        'airflow.plugins': [
            'slurm = slurm_executor.slurm:SlurmExecutorPlugin'
        ]
    }
)

